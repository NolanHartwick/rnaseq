import pandas
import os
import json


def handle_config(config):
    """ Config will be read and checked for validity. Returns a new dictionary with
        default values added where applicable.
    """
    # Reads must always be provided
    reads = config["reads"]
    read_type = config["read_type"].lower()

    # Define optional args
    ref = config.get("fasta")
    transcripts = config.get("transcripts")
    gene_trans_map = config.get("gene_map")
    gff = config.get("gff")
    names = config.get("names")
    strand = config.get("strand", "u").lower()
    opts = config.get("opts", {})
    quant_type = config.get("quant_type", "salmon")
    plot_params = config.get("plot_params", {})

    # check that read_type is valid
    valid_read_types = {"long", "pe", "short"}
    if(read_type not in valid_read_types):
        raise ValueError(f"read_type not understood. Should be one of: {valid_read_types}")

    # Provide default names if not given
    if(names is None):
        if(config["read_type"] == "pe"):
            def_names = [d["r1"] for d in config["reads"]]
        elif(config["read_type"] == "short"):
            def_names = config["reads"]
        elif(config["read_type"] == "long"):
            def_names = config["reads"]
        else:
            raise ValueError("the fuck")
        prefix = os.path.commonprefix(def_names)
        prefix = prefix[:-1 * len(os.path.split(prefix)[1])]
        def_names = [r[len(prefix):] for r in def_names]
        names = def_names

    # ensure name length matches number of samples
    if(len(names) != len(config["reads"])):
        raise ValueError("Provided set of reads and names have different lengths.")

    # validate strand info
    strand_options = {"u", "fr", "rf", "f", "r"}
    if(strand not in strand_options):
        raise ValueError(f"Provided strand option ({strand}) isn't in : {strand_options}")

    # check that some combination of valid inputs is provided
    if(transcripts is None and ref is None):
        raise ValueError(
            "No reference provided. Reference should be provided in the form "
            "of a genome fasta or transcripts fasta"
        )

    # check that provided paths are valid
    check_paths = {"gff": gff, "ref": ref, "transcripts": transcripts}
    for n, p in check_paths.items():
        if(p is not None and not os.path.exists(p)):
            raise ValueError(f"{n} not found : {p}")

    ret = {
        "reads": reads,
        "read_type": read_type,
        "fasta": ref,
        "gff": gff,
        "transcripts": transcripts,
        "gene_map": gene_trans_map,
        "opts": opts,
        "names": names,
        "strand": strand,
        "quant_type": quant_type,
        "plot_params": plot_params
    }

    # dump config for future reference
    with open("config.json", "w") as fout:
        json.dump(ret, fout, indent=4)

    return ret


def load_stats(fpath):
    """ Read a samtools stats output file and collect statistics
    """
    with open(fpath) as f:
        sn_lines = []
        for line in f:
            if(line.startswith("SN")):
                sn_lines.append(line)
    sn_data = [l.split("\t") for l in sn_lines]
    sn_data = {
        k.strip().rstrip(":"): float(v)
        for _, k, v, *_ in sn_data
    }
    return sn_data


def merge_stats(input, outpath):
    """ Reads basic stats from samtools and reformats them into a single table
    """
    bases = [os.path.basename(f).split(".")[0] for f in input]
    all_stats = [load_stats(f) for f in input]
    pandas.DataFrame(dict(zip(bases, all_stats))).to_csv(outpath, sep="\t")


def split_gff(infile, strand_out, unstrand_out):
    """ Splits a gff file into two files, one containing stranded calls and
        one containing unstranded calls
    """
    df = pandas.read_csv(infile, sep="\t", comment="#", header=None)
    stranded = df[df[6] != "."]
    unstranded = df[df[6] == "."]
    stranded.to_csv(strand_out, header=None, index=None, sep="\t")
    unstranded.to_csv(unstrand_out, header=None, index=None, sep="\t")


def rename_FC(infile, outfile, names):
    """ Parses feature counts output and renames columns using provided names
    """
    df = pandas.read_csv(infile, sep="\t", comment="#")
    df = df.set_index("Geneid")
    df = df.iloc[:, -1 * len(names):]
    df.columns = names
    df.to_csv(outfile, sep="\t")


def load_gff(file_path, dropna):
    """ Read a gff file and return it as a pandas dataframe

        Params -
            file_path
                the path to the gff file
            dropna
                drop rows from gff file which lack seqname, source, feature,
                start, or end information.
        Returns - Pandas.DataFrame
            A dataframe of shape (num_entries, 9)
    """
    # define colnames
    colnames = [
        'seqname', 'source', 'feature',
        'start', 'end', 'score',
        'strand', 'frame', 'attribute'
    ]
    # parse the gtf file
    ret_data = pandas.read_csv(
        file_path,
        sep='\t',
        comment="#",
        header=None,
        names=colnames
    )
    if(dropna):
        ret_data = ret_data.dropna(how="any", subset=colnames[:5])
    ret_data.start = ret_data.start.astype(int, errors="ignore")
    ret_data.end = ret_data.end.astype(int, errors="ignore")
    # parse attribute column to dict
    ret_data["attribute"] = ret_data.attribute.str.split(";|=").apply(
        lambda vals: dict(vals[::2], vals[1::2])
    )
    return ret_data


def reverse_map(d):
    """ Given a dictionary mapping keys to a set of values, return
        the reverse mapping from a value to a set of keys

        >>> a = {0: {"a", "b"}}
        >>> reverse_map(a) == {"a": {0}, "b": {0}}
        True
    """
    ret = {v: set() for k, s in d.items() for v in s}
    for k, s in d.items():
        for v in s:
            ret[v].add(k)
    return ret


def make_gene_mapping(gff, outpath):
    """ parse a gff3 file and use parent relationships to create a mapping
        between gene ids and transcript ids
    """
    gff = load_gff(gff)
    transcripts = gff[gff.feature == "mRNA"]
    trans_to_genes = {atr["ID"]: {atr["Parent"]} for atr in transcripts.attribute}
    gene_to_trans = reverse_map(trans_to_genes)
    with open(outpath, "w") as fout:
        json.dump(gene_to_trans, fout, indent=4)


def merge_salmon(quants, raw_out, cpm_out, names):
    """ merges output from salmon quants into 4 files representing both gene
        and transcript level raw and cpm counts.
    """
    quants = [pandas.read_csv(f, sep="\t").set_index("Name") for f in quants]
    tpm = pandas.DataFrame({n: df.TPM for n, df in zip(names, quants)})
    tpm.to_csv(cpm_out, sep="\t")
    raw = pandas.DataFrame({n: df.NumReads for n, df in zip(names, quants)})
    raw.to_csv(raw_out, sep="\t")




if(__name__ == "__main__"):
    # TODO: Impliment tests
    # import doctest
    # doctest.testmod()
    pass
