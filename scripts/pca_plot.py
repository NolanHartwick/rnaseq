import pandas
from sklearn.decomposition import PCA
from plotly import express
from plotly import graph_objects as go
from plotly import offline
from scipy.stats import zscore


def stdnorm(df):
    """ Applies zscore to the rows (genes) of df
    """
    ret = df.transpose().apply(zscore).transpose()
    ret = ret.dropna()
    return ret


def pca_plot(df, outpath, title, colors, symbols):
    reducer = PCA(n_components=2)
    reduced = reducer.fit_transform(df.transpose())
    reduced = pandas.DataFrame(
        reduced,
        columns=['pc1', 'pc2'],
    )
    reduced["labels"] = df.columns
    reduced["symbols"] = symbols
    reduced["colors"] = colors
    fig = express.scatter(
        reduced,
        x="pc1",
        y="pc2",
        hover_data=["labels"],
        color=None if colors is None else "colors",
        symbol=None if symbols is None else "symbols",
    )
    fig.update_layout(title=title)
    offline.plot(fig, filename=outpath, auto_open=False)


def corr_plot(df, outpath, title):
    cor = df.corr()
    fig = go.Figure(data=go.Heatmap(
        x=cor.index,
        y=cor.index,
        z=cor.values,
        type="heatmap",
        colorscale="Viridis",
        zmin=-1,
        zmax=1
    ))
    fig.update_layout(title=title)
    offline.plot(fig, filename=outpath, auto_open=False)


def main(
    counts_path,
    title,
    pca_out,
    corr_out,
    tpm=False,
    standardize=False,
    pca_colors=None,
    pca_symbols=None
):
    """ Main method for module

        counts_path : A path to the counts file produced by rename_FC
        title : a title used for both plots
        pca_out : a list of file paths for the generated pca plots
        corr_out : a list of file paths for the generated heatmaps
        tpm : Wether or not to tpm normalize the data
        standardize : whether or not to stanadardize expression at the gene level
        pca_colors: optional list of ids used to inform coloring of pca points
        pca_symbols: optional list of ids used to inform pca point symbols
    """
    counts = pandas.read_csv(counts_path, sep="\t", index_col=0)
    if(tpm):
        counts = counts / counts.sum() * 10 ** 6
    if(standardize):
        counts = stdnorm(counts)
    counts = counts.dropna()
    pca_plot(counts, pca_out, title, pca_colors, pca_symbols)
    corr_plot(counts, corr_out, title)


def snake_main():
    """ Isolates snakemake specific magic, parsing needed options and invoking
        main numerous times.
    """
    counts_path = snakemake.input[0]
    pca_plot = [p for p in snakemake.output["pcas"]]
    corr_plot = [p for p in snakemake.output["cors"]]
    pca_colors = snakemake.params.pca_colors
    pca_symbols = snakemake.params.pca_symbols
    tpms = snakemake.params.tpm
    standardize = snakemake.params.standardize
    titles = snakemake.params.titles
    for pp, cp, tpm, std, tit in zip(pca_plot, corr_plot, tpms, standardize, titles):
        main(counts_path, tit, pp, cp, tpm, std, pca_colors, pca_symbols)


if(__name__ == "__main__"):
    snake_main()
