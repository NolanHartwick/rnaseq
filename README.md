# Snakemake RNAseq

This snakemake worflow impliments the steps needed to estimate expression for an RNAseq experiment. It will adapt itself to work for short paired/unpaired reads or long reads as appropriate.

### General Pipeline

Reads are first mapped to the reference genome using either minimap2 for long reads or hisat2 for short reads. If no genome annotation was provided, these alignments will be passed to stringtie for assembling and to transdecoder for orf predictions. The resultant annotation or the provided annotation is then passed to FeatureCounts along with the bam files for quantification.

### Examples

Simple exeuction using long reads and without a pre-existing annotation. Reads will be assumed to be unstranded.

```
$cat config.json
{
    "fasta": "assembly.fasta",
    "reads": [
        "sample1.fastq.gz",
        "sample2.fastq.gz",
        "sample3.fastq.gz",
        "sample4.fastq.gz"
    ],
    "read_type": "long"
}
$snakemake -p -j 48 --use-conda --configfile config.json --directory /path/to/output/directory

```

### Test install

You can install and partially test this workflow using the following commands, assuming you have conda installed...

```
git clone https://gitlab.com/NolanHartwick/rnaseq.git
cd rnaseq
conda env create -f envs/rnaseq.yml
conda activate snake_counts
snakemake --configfile example_data/config.json -d example_data/example_outdir -j 4 -p
```

This will use salmon to align some (750 reads) miRNA data to a reference and then quantify using these reads. The output folder will be generated at "example_data/example_outdir".
