import os
import pandas
import json
from scripts import util


config = util.handle_config(config)
opts = config["opts"]


os.makedirs("plots", exist_ok=True)


rule all:
    input:
        ".completed.flag"

if(config["fasta"] is not None):
    rule hisat2_index:
        input:
            config["fasta"]
        output:
            os.path.join("hisat2_index", "reference.1.ht2")
        conda:
            "envs/rnaseq.yml"
        params:
            outname = os.path.join("hisat2_index", "reference"),
            opt = opts.get("hisat2_index", "")
        shell:
            "hisat2-build {params[opt]} {input[0]} {params[outname]}"


    hisat2_strand_map = {
        "u": "",
        "rf": "--rna-strandness RF",
        "fr": "--rna-strandness FR",
        "r": "--rna-strandness r",
        "f": "--rna-strandness f"
    }

    # only applies to paired_end type reads
    rule hisat2_align_pe:
        input:
            ref = rules.hisat2_index.output[0],
            r1 = lambda wcs: config["reads"][int(wcs["n"])]["r1"],
            r2 = lambda wcs: config["reads"][int(wcs["n"])]["r2"],
        output:
            [temp(os.path.join("alignments", "reads_{n}.pe.sam"))]
        conda:
            "envs/rnaseq.yml"
        threads:
            8
        params:
            idx_name = rules.hisat2_index.params[0],
            strand = hisat2_strand_map[config["strand"]],
            opt = opts.get("hisat2_align_pe", "")
        shell:
            "hisat2 -p {threads} {params[strand]} {params[opt]} --dta-cufflinks -x {params[idx_name]} -1 {input[r1]} -2 {input[r2]} -S {output[0]}"


    # only applies to unpaired/single end reads
    rule hisat2_align_unpaired:
        input:
            ref = rules.hisat2_index.output[0],
            ureads = lambda wcs: config["reads"][int(wcs["n"])]
        output:
            [temp(os.path.join("alignments", "reads_{n}.short.sam"))]
        conda:
            "envs/rnaseq.yml"
        threads:
            8
        params:
            idx_name = rules.hisat2_index.params[0],
            strand = hisat2_strand_map[config["strand"]],
            opt = opts.get("hisat2_align_unpaired", "")
        shell:
            "hisat2 -p {threads} {params[strand]} {params[opt]} --dta-cufflinks -x {params[idx_name]} -u {input[ureads]} -S {output[0]}"


    minimap_strand_map = {
        "u": "",
        "f": "-uf",
        "fr": "ERROR",
        "rf": "ERROR",
        "r": "ERROR"
    }

    # use minimap2 to align long reads to genome
    rule minimap2_long:
        input:
            ref = config["fasta"],
            lreads = lambda wcs: config["reads"][int(wcs["n"])]
        output:
            [temp(os.path.join("alignments", "reads_{n}.long.sam"))]
        conda:
            "envs/rnaseq.yml"
        threads:
            8
        params:
            strand = minimap_strand_map[config["strand"]],
            opt = opts.get("minimap2_long", "")
        shell:
            "minimap2  -t {threads} {params[strand]} -ax splice {params[opt]} {input[ref]} {input[lreads]} > {output[0]}"


    rule sam_to_bam:
        input:
            ["{basename}.sam"]
        output:
            ["{basename}.sorted.bam"]
        conda:
            "envs/rnaseq.yml"
        threads:
            8
        priority: 10
        shell:
            "samtools view -u {input[0]} | samtools sort -@ {threads} -o {output[0]}"


    rule bam_stats:
        input:
            ["{basename}.sorted.bam"]
        output:
            ["{basename}.stats"]
        conda:
            "envs/rnaseq.yml"
        shell:
            "samtools stats {input[0]} > {output[0]}"


    def merge_stats_input(wcs):
        template = {
            "short": os.path.join("alignments", "reads_{n}.short.stats"),
            "pe": os.path.join("alignments", "reads_{n}.pe.stats"),
            "long": os.path.join("alignments", "reads_{n}.long.stats"),
        }[config["read_type"]]
        return [template.format(n=n) for n, _ in enumerate(config["reads"])]


    rule merge_stats:
        input:
            merge_stats_input
        output:
            [os.path.join("alignment_stats.tsv")]
        run:
            util.merge_stats(input, output[0])

    stringtie_strand_map = {
        "u": "",
        "rf": "--rf",
        "fr": "--fr",
        "r": "--rf",
        "f": "--fr"
    }

    rule stringtie_assemble:
        input:
            "{reads}.sorted.bam"
        output:
            "{reads}.gtf"
        conda:
            "envs/rnaseq.yml"
        threads:
            16
        params:
            lflag = "-L" if config["read_type"] == "long_reads" else "",
            strand = stringtie_strand_map[config["strand"]],
            opt = opts.get("stringtie_assemble", "")
        shell:
            "stringtie {params[opt]} {params[strand]} {params[lflag]} -p {threads} {input} -o {output}"


    def stringtie_merge_input(wcs):
        template = {
            "short": os.path.join("alignments", "reads_{n}.short.gtf"),
            "pe": os.path.join("alignments", "reads_{n}.pe.gtf"),
            "long": os.path.join("alignments", "reads_{n}.long.gtf"),
        }[config["read_type"]]
        return [template.format(n=n) for n, _ in enumerate(config["reads"])]


    rule stringtie_merge:
        input:
            stringtie_merge_input
        output:
            "stringtie.gtf"
        conda:
            "envs/rnaseq.yml"
        params:
            # l_opt = "-L" if config["read_type"] == "long_reads" else "",
            strand = stringtie_strand_map[config["strand"]],
            opt = opts.get("stringtie_merge", "")
        shell:
            "stringtie {params[opt]} {params[strand]} --merge -o {output} {input}"


    rule split_stringtie:
        input:
            rules.stringtie_merge.output
        output:
            temp("stringtie.stranded.gtf"),
            temp("stringtie.unstranded.gtf"),
        run:
            util.split_gff(input[0], output[0], output[1])


    rule get_transcripts_transdecoder:
        input:
            "stringtie.{strand}.gtf",
            config["fasta"]
        output:
            "stringtie.{strand}.transcripts.fasta"
        conda:
            "envs/rnaseq.yml"
        shell:
            "gtf_genome_to_cdna_fasta.pl {input} > {output}"


    rule LongOrfs:
        input:
            rules.get_transcripts_transdecoder.output
        output:
            "stringtie.{strand}.transcripts.fasta.transdecoder_dir/longest_orfs.gff3"
        params:
            s = lambda wcs: "-S" if wcs["strand"] == "stranded" else ""
        conda:
            "envs/rnaseq.yml"
        shell:
            "TransDecoder.LongOrfs {params[s]} -t {input}"


    rule PredictOrfs:
        input:
            rules.get_transcripts_transdecoder.output[0],
            rules.LongOrfs.output[0]
        output:
            [
                temp(f"stringtie.{{strand}}.transcripts.fasta.transdecoder.{ext}")
                for ext in ["gff3", "cds", "bed", "pep"]
            ]
        conda:
            "envs/rnaseq.yml"
        shell:
            "TransDecoder.Predict -t {input[0]}"


    rule transecoder_gtf_to_gff3:
        input:
            "stringtie.gtf"
        output:
            temp("stringtie.gff3")
        conda:
            "envs/rnaseq.yml"
        shell:
            "gtf_to_alignment_gff3.pl {input} > {output}"


    rule merge_transdecoder:
        input:
            rules.PredictOrfs.output[0],
            rules.transecoder_gtf_to_gff3.output[0],
            rules.get_transcripts_transdecoder.output[0]
        output:
            temp("stringtie.{strand}.transdecoder.gff3")
        conda:
            "envs/rnaseq.yml"
        shell:
            """
            echo '##gff-version 3' > {output}
            cdna_alignment_orf_to_genome_orf.pl {input} >> {output}
            """


    rule gt_sort:
        input:
            rules.merge_transdecoder.output
        output:
            temp("stringtie.{strand}.transdecoder.sorted.gff3")
        conda:
            "envs/rnaseq.yml"
        shell:
            "gt gff3 -sort -retainids {input} > {output}"


    def gt_merge_input(wcs):
        ret = ["stringtie.stranded.transdecoder.sorted.gff3"]
        # if input is unstranded, grab the unstranded transdecoder predictions
        if(config["strand"] == "u"):
            ret.append("stringtie.unstranded.transdecoder.sorted.gff3")
        return ret


    rule gt_merge:
        input:
            gt_merge_input
        output:
            "stringtie.transdecoder.genome.gff3"
        conda:
            "envs/rnaseq.yml"
        shell:
            "gt merge -retainids -o {output} {input}"



    def feature_counts_input(wcs):
        template = {
            "short": os.path.join("alignments", "reads_{n}.short.sorted.bam"),
            "pe": os.path.join("alignments", "reads_{n}.pe.sorted.bam"),
            "long": os.path.join("alignments", "reads_{n}.long.sorted.bam"),
        }[config["read_type"]]
        return {
            "bam": [template.format(n=n) for n, _ in enumerate(config["reads"])],
            "annot": config["gff"] if(config["gff"] is not None) else rules.gt_merge.output[0]
        }


    rule feature_counts:
        input:
            unpack(feature_counts_input)
        output:
            f"feature_counts.fc.tsv"
        params:
            rt = "-L" if config["read_type"] == "long" else "",
            opt = opts.get("feature_counts", "-t gene -f -g ID -M -O")
        conda:
            "envs/rnaseq.yml"
        shell:
            "featureCounts {params[rt]} {params[opt]} -o {output} -a {input[annot]} {input[bam]}"


    rule rename_fc:
        input:
            rules.feature_counts.output[0]
        output:
            f"genes.raw.tsv"
        run:
            util.rename_FC(input[0], output[0], config["names"])


    rule gt_get_transcripts:
        input:
            config["gff"] if(config["gff"] is not None) else rules.gt_merge.output[0],
            config["fasta"]
        output:
            f"mRNA.fasta"
        conda:
            "envs/rnaseq.yml"
        shell:
            "gt extractfeat -type exon -join yes -matchdescstart yes --retainids yes -seqfile {input[1]} {input[0]} > {output}"


rule salmon_index:
    input:
        rules.gt_get_transcripts.output[0] if(config["transcripts"] is None) else config["transcripts"]
    output:
        directory("salmon_index")
    conda:
        "envs/rnaseq.yml"
    params:
        opt = opts.get("salmon_index", "")
    shell:
        "salmon index {params[opt]} -t {input} -i {output}"


rule salmon_quant_pe:
    input:
        ref = rules.salmon_index.output[0],
        r1 = lambda wcs: config["reads"][int(wcs["n"])]["r1"],
        r2 = lambda wcs: config["reads"][int(wcs["n"])]["r2"],
    output:
        "alignments/salmon_{n}.pe/quant.sf"
    conda:
        "envs/rnaseq.yml"
    threads:
        8
    params:
        opt = opts.get("salmon_quant_pe", "-l A "),
        out = "alignments/salmon_{n}.pe"
    shell:
        "salmon quant {params[opt]} -i {input[ref]} -1 {input[r1]} -2 {input[r2]} -o {params[out]}"


rule salmon_quant_unpaired:
    input:
        ref = rules.salmon_index.output[0],
        ureads = lambda wcs: config["reads"][int(wcs["n"])]
    output:
        "alignments/salmon_{n}.short/quant.sf"
    conda:
        "envs/rnaseq.yml"
    threads:
        8
    params:
        opt = opts.get("salmon_quant_unpaired", "-l A "),
        out = "alignments/salmon_{n}.short"
    shell:
        "salmon quant {params[opt]} -i {input[ref]} -r {input[ureads]} -o {params[out]}"


rule minimap2_transcripts:
    input:
        ref = rules.gt_get_transcripts.output[0] if(config["transcripts"] is None) else config["transcripts"],
        ureads = lambda wcs: config["long_reads"][int(wcs["n"])]
    output:
        "alignments/reads_{n}.transcripts.bam"
    conda:
        "envs/rnaseq.yml"
    threads:
        8
    params:
        opt = opts.get("minimap2_transcripts", "")
    shell:
        "minimap2 -ax splice {params[opt]} {input[0]} {input[1]} | samtools view -S -b > {output}"


rule salmon_quant_long:
    input:
        ref = rules.salmon_index.output[0],
        mapping = rules.minimap2_transcripts.output[0]
    output:
        "alignments/salmon_{n}.long/quant.sf"
    conda:
        "envs/rnaseq.yml"
    threads:
        8
    params:
        opt = opts.get("salmon_quant_long", "-l A "),
        out = "alignments/salmon_{n}.long"
    shell:
        "salmon quant {params[opt]} -i {input[ref]} -b {input[mapping]} -o {params[out]}"


def merge_salmon_input(wcs):
    template = {
        "short": rules.salmon_quant_unpaired.output[0],
        "pe": rules.salmon_quant_pe.output[0],
        "long": rules.salmon_quant_long.output[0],
    }[config["read_type"]]
    return [template.format(n=n) for n, _ in enumerate(config["reads"])]


rule merge_salmon:
    input:
        merge_salmon_input
    output:
        f"salmon.counts.tsv",
        f"salmon.cpm.tsv"
    run:
        util.merge_salmon(input, output[0], output[1], config["names"])


norms = ["std", "tpm", "std_tpm", "raw"]

rule visualize:
    input:
        rules.merge_salmon.output[0] if(config["quant_type"] == "salmon") else rules.rename_fc.output[0]
    output:
        pcas = [f"plots/pca.{n}.html" for n in norms],
        cors = [f"plots/cor.{n}.html" for n in norms]
    conda:
        "envs/rnaseq.yml"
    params:
        standardize = ["std" in n for n in norms],
        tpm = ["tpm" in n for n in norms],
        titles = [f"Counts with norm {n}" for n in norms],
        pca_colors = config["plot_params"].get("colors"),
        pca_symbols = config["plot_params"].get("symbol")
    script:
        "scripts/pca_plot.py"


def get_targets(wcs):
    ret = []
    if(config["quant_type"] == "salmon"):
        ret.extend(rules.merge_salmon.output)
    else:
        ret.extend(rules.rename_fc.output)
    if(config["quant_type"] != "salmon" or config["transcripts"] is None):
        ret.extend(rules.merge_stats.output)
    if(len(config["reads"]) > 1):
        ret.extend(rules.visualize.output)
    return ret


rule finalize:
    input:
        unpack(get_targets)
    output:
        rules.all.input[0]
    shell:
        "touch {output}"
